import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {InputRequestComponent} from './components/input-request/input-request.component';
import {TimeService} from "./services/timeService/time.service";
import {ConnectionsService} from "./services/connectionService/connection.service";
import {EchoService} from "./services/echoService/echo.service";
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import {webServerURL} from './../environments/environment'
import {HttpClientModule} from "@angular/common/http";
import { ButtonsRequestComponent } from './components/buttons-request/buttons-request.component';
const config: SocketIoConfig = { url: webServerURL, options: {} };

@NgModule({
    declarations: [
        AppComponent,
        InputRequestComponent,
        ButtonsRequestComponent,

    ],
    imports: [
        BrowserModule,
        AppRoutingModule, SocketIoModule.forRoot(config) ,HttpClientModule

    ],
    providers: [
        TimeService,
        ConnectionsService,
        EchoService,



    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
