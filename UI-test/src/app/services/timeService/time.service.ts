import {Injectable} from '@angular/core';
import {ConnectionsService} from "../connectionService/connection.service";
import {BehaviorSubject} from "rxjs/Rx";

@Injectable()
export class TimeService {

    public bSubject = new BehaviorSubject(undefined);

    constructor(private connectionsService: ConnectionsService) {
        this.startGetTime();

    }

    private startGetTime = () => {
        this.connectionsService.getMessage('time').subscribe((data) => {
            this.bSubject.next(data);
        });
    }
}
