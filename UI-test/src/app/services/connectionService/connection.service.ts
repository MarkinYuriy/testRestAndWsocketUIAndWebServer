import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Socket} from 'ngx-socket-io';
type MAP = {[key: string]: any};
import {webServerURL} from './../../../environments/environment';

@Injectable()
export class ConnectionsService {

    private header: any = {'Content-Type': 'application/json'};
    constructor(private http: HttpClient, private socket_io: Socket) {

    }


    private getUrl(params, method = 'get') {
        return `${webServerURL}/general/${method}?${params}`;
    }

    private getHeaders(headers) {
        return {'Content-Type': 'application/json', ...headers};
    }

    public get(params) {
        return this.http.get(this.getUrl(params, 'get'));
    }

    public put(params, body, headers = {}) {
        return this.http.put(this.getUrl(params, 'put'), body, this.getHeaders(headers));
    }

    public delete(params) {
        return this.http.delete(this.getUrl(params, 'delete'));
    }

    public post(body) {
        return this.http.post(this.getUrl('', 'post'), body, this.header);
    }
    public sendWSocketIO(msg: string, room: string) {
        this.socket_io.emit(room, msg);
    }

    public getMessage(room: string) {
        return this.socket_io
            .fromEvent(room);

    }

}
