import {Injectable} from '@angular/core';
import {ConnectionsService} from "../connectionService/connection.service";
import {BehaviorSubject} from "rxjs/Rx";

@Injectable()
export class EchoService {

    public bSubject = new BehaviorSubject(undefined);

    constructor(private connectioService: ConnectionsService) {
        this.startGetEcho();

    }

    private startGetEcho = () => {
        this.connectioService.getMessage('echo').subscribe((data) => {
            this.bSubject.next(data);
        });
    }


    public sendEcho = (message) => {
        this.connectioService.sendWSocketIO(message,'echo');
    }
}