import {Component, OnInit} from '@angular/core';
import {TimeService} from "../../services/timeService/time.service";
import {EchoService} from "../../services/echoService/echo.service";

@Component({
    selector: 'app-input-request',
    templateUrl: './input-request.component.html',
    styleUrls: ['./input-request.component.scss']
})
export class InputRequestComponent implements OnInit {
    data: any = {time: '', echo: ''};

    constructor(private timeService: TimeService, private  echoService: EchoService) {

    }

    ngOnInit() {
        this.timeService.bSubject.subscribe((data) => {
            this.data.time = (data) ? JSON.stringify(data) : '';
        });
        this.echoService.bSubject.subscribe((data) => {
            this.data.echo = (data) ? JSON.stringify(data) : '';
        });
    }

    public sendEcho(message) {
        try{
            message = JSON.parse(message);
        }catch (e){

        }
        this.echoService.sendEcho(message);
    }

}
