import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputRequestComponent } from './input-request.component';

describe('InputRequestComponent', () => {
  let component: InputRequestComponent;
  let fixture: ComponentFixture<InputRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
