import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonsRequestComponent } from './buttons-request.component';

describe('ButtonsRequestComponent', () => {
  let component: ButtonsRequestComponent;
  let fixture: ComponentFixture<ButtonsRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ButtonsRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonsRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
