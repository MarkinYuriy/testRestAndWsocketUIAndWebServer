import { Component, OnInit } from '@angular/core';
import {ConnectionsService} from "../../services/connectionService/connection.service";

@Component({
  selector: 'app-buttons-request',
  templateUrl: './buttons-request.component.html',
  styleUrls: ['./buttons-request.component.scss']
})
export class ButtonsRequestComponent implements OnInit {

  isGet: boolean = false;
  isPost: boolean = false;
  isPut: boolean = false;
  isDelete: boolean = false;
  response;
  params: string= 'ggg=gf';
  body = {body: 'body'};
  bodyString = JSON.stringify(this.body);


  constructor(private connectionService: ConnectionsService ) { }

  ngOnInit() {
  }
  sendGet(){
    if(this.isGet === false){
      this.isGet = true;
      this.connectionService.get(this.params)
        .subscribe((response) => {
        this.response = JSON.stringify(response);
        console.log(response);
      })
    }else{
      this.isGet = false;
    }

  }
  sendPost(){
    if(this.isPost === false){
      this.isPost = true;
      this.connectionService.post(this.body)
        .subscribe((response) => {
        this.response = JSON.stringify(response);
        console.log(response);
      })
    }else{
      this.isPost = false;
    }
  }
  sendPut(){
    if(this.isPut === false){
      this.isPut = true;
      this.connectionService.put(this.params, this.body)
        .subscribe((response) => {
        this.response = JSON.stringify(response);
        console.log(response);
      })
    }else{
      this.isPut = false;
    }
  }
  sendDelete(){
    if(this.isDelete === false){
      this.isDelete = true;
      this.connectionService.delete(this.params).subscribe((response) => {
        this.response = JSON.stringify(response);
        console.log(response);
      })
    }else{
      this.isDelete = false;
    }
  }


}
