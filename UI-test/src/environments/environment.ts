export const environment = {
  production: false
};

export const webServerURL = 'http://localhost:3456';
export const webSocketRooms = ['echo,time'];
