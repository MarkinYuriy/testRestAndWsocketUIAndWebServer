const gulp = require('gulp');
const ts = require('gulp-typescript');
const JSON_FILES = ['src/*.json', 'src/**/*.json','src/*.js', 'src/**/*.js','src/**/*.jpg','src/**/*.png'];

const tsProject = ts.createProject('tsconfig.json');

gulp.task('scripts',['assets'], function () {
    const tsResult = tsProject.src()
        .pipe(tsProject());
    return tsResult.js.pipe(gulp.dest('dist'));
})
;

gulp.task('watch', ['scripts'], function () {
    gulp.watch('src/**/*.ts', ['scripts']);
    gulp.watch('src/**/*.js', ['scripts']);
})
;



gulp.task('assets', function () {
    return gulp.src(JSON_FILES)
        .pipe(gulp.dest('dist'));
});

gulp.task('default', ['watch', 'assets']);