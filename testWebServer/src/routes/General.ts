import {Router, Request, Response, NextFunction} from 'express';

const _ = require("lodash");
// import {PrepareUavData} from "../dataProcessing/prepareUavData";

type MAP = {[key: string]: any};

export class GeneralRouter {
    router: Router;


    constructor() {
        this.router = Router();
        this.init();
    }


    public get = (req: Request, res: Response, next: NextFunction) => {
        if (req.method == "GET") {
            let query: string = this.buildParamsFromQuery(req.query);
             res.status(200).send({"status":"success"});
        }
        else {
            res.status(405).send({error: 'Method Not Allowed'});
        }

    };

    public put = (req: Request, res: Response, next: NextFunction) => {
        if (req.method == "PUT") {
            let query: string = this.buildParamsFromQuery(req.query);
            let body = req.body;
            res.status(200).send({"status":"success"});
        } else {
            res.status(405).send({error: 'Method Not Allowed'});

        }

    };
    public post = (req: Request, res: Response, next: NextFunction) => {
        if (req.method == "POST") {
            let query: string = this.buildParamsFromQuery(req.query);
            let body = req.body;
            res.status(200).send({"status":"success"});
        } else {
            res.status(405).send({error: 'Method Not Allowed'});
        }
    };
    public delete = (req: Request, res: Response, next: NextFunction) => {
        if (req.method == "DELETE") {
            let query: string = this.buildParamsFromQuery(req.query);
            res.status(200).send({"status":"success"});
            res.status(200).send({});
        } else {
            res.status(405).send({error: 'Method Not Allowed'});

        }

    };

    buildParamsFromQuery(params: string[]): string {
        let query: string = '';
        if (params.length > 0) {
            query = '?';
            for (let prop in params) {
                query += prop + '=' + params[prop] + '&'
            }

            if (query[query.length - 1] == '&') {
                query = query.slice(0, -1);
            }
        }
        return query
    }

    init() {
        this.router.get('/get', this.get);
        this.router.get('/put', this.put);
        this.router.get('/post', this.post);
        this.router.get('/delete', this.delete);
        console.log('Upped router (CRUD)');
    };




}

