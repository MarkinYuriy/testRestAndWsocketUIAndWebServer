import * as express from "express";
import * as http from "http";
import * as socketIo from "socket.io";
import * as bodyParser from "body-parser";
import {GeneralRouter} from "./routes/General";
import * as cors from "cors";

const fs = require('fs');
const path = require('path');
const projConf = require(path.join(__dirname, './../../projConfTest.json'));


class Server {

    private port = projConf.webServer.port;

    public app: any;
    private server: any;
    private webSocket: any;


    public static bootstrap(): Server {
        return new Server();
    }


    constructor() {
        this.createApp();
        this.createServer();
        this.middleware();
        this.listen();
        this.routes();
        this.startWebsocket();

    }

    private startWebsocket() {
        this.webSocket = socketIo.listen(this.server);
        this.webSocket.on('connect', (socket: any) => {
            //=============?????????????==============
            socket.on('echo', (data) => {

                this.webSocket.emit('echo', {youSent: data});

            });

            setInterval(() => {
                this.webSocket.emit('time', {myTime: new Date(Math.floor((new Date()).getTime() / 1000) * 1000).toISOString()});
            }, 1000)

            //========================================


        });
    }

    //===================         Configure Express middleware.      ====================
    private middleware(): void {
        this.app.use(bodyParser.json({limit: '50mb'}));
        this.app.use(bodyParser.urlencoded({extended: false, limit: '50mb'}));
        // enable cors middleware
        const options: cors.CorsOptions = {
            allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
            credentials: true,
            methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
            origin: "*",
            preflightContinue: false
        };
        this.app.use(cors(options));
        this.app.options("*", cors(options));
    }

    // Configure API endpoints.
    private routes(): void {
        this.app.use('/', express.static(path.join(__dirname, '/public')));

        let routeInstance = new GeneralRouter();
        const listMethods = ['get', 'put', 'post', 'delete'];
        listMethods.forEach((method) => {
            this.app.use('/general/' + method, routeInstance[method]);
        })
    }

    private createApp(): void {
        this.app = express();
    }

    private createServer(): void {
        this.server = http.createServer(this.app);
    }

    public listen(): any {
        this.server.listen(this.port, () => {
            console.log('Running server on port ', this.port);
        });
    }

    //===================================================================================


}

// let server =
Server.bootstrap();
